var fs = require('fs')
var express = require('express')
var app = express()
var port = 25555
var middleware = require('socketio-wildcard')();
var cors = require('cors');
var ex = require('./t');

var options = {
  key: fs.readFileSync('/etc/letsencrypt/live/dev.trikthom.com/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/dev.trikthom.com/cert.pem'),
  ca: fs.readFileSync('/etc/letsencrypt/live/dev.trikthom.com/chain.pem')
  //rejectUnauthorized: false
};

var server = require('https').Server(options, app)
var io = require('socket.io')(server)


server.listen(port, () => {
  console.log("Express server listening on port " + port);
});

var savedData = []
var objects = []
var defaultSettings = {
  saveSuccessfulToVault: false,
  saveAllToVault: false
}
var settings = defaultSettings

var testerId = null

var logger = fs.createWriteStream('log.txt', {
  flags: 'a' // 'a' means appending (old data will be preserved)
})

app.get('/tests', (req, res) => {
  res.json(savedData);
});

app.get('/console', (req, res) => {
  res.sendFile(__dirname + '/console.html');
});

app.use(cors({
  origin: 'https://wrijving.trikthom.com'
}))

app.get('/excel', (req, res) => {
  // You can define styles as json object
  ex.export(res, savedData)
});
app.get('*', (req, res) => {
  res.json({error: req.originalUrl + ' not found'})
})

function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/\s+/g, '');
}

io.use(middleware);

io.on('connection', function(socket){

  socket.on("*",function(event,data) {
    if (event.data) logger.write(JSON.stringify(event.data) + '\r\n')
    console.log(event)
  })

  console.log("Socket Connected!");

  socket.emit('tests', savedData.filter(a => a.success != undefined))

  socket.emit('objects', objects)

  socket.emit('settings', settings)

  socket.on('startNewTest', (data, fn) => {
    data.id = savedData.length
    savedData.push(data)
    console.log(savedData)
    socket.emit('testStarting', data)
    socket.broadcast.emit('testStarting', data)
    var log = 'Starting test ' + data.id
    socket.broadcast.emit('log', log)
    socket.emit('log', log)
  })

  socket.on('testStarted', (id) => {
    var log = 'Test ' + id + ' started'
    socket.broadcast.emit('log', log)
    socket.emit('log', log)
    console.log('test ' + id + ' has started')
  })

  socket.on('addValuesToTest', (data) => {
  console.log(data);
    data.forEach((d) => {
      if(savedData[d.id] != null) {
        savedData[d.id][d.name] = d.value
      } else console.log('test doesn\'t excists!')
    })
  })

  socket.on('testEnded', (id) => {
    console.log('test ' + id + ' ended')
    calculate(id)
  })

  function calculate(id) {
    var log = 'Calculating...'
    socket.broadcast.emit('log', log)
    socket.emit('log', log)

    var d = savedData[id]
    /*
    d.t = d.t1 - d.t0
    d.v = s / d.t
    d.fz = d.m * 9.81
    d.fw = d.fz * Math.sin(d.alpha * (Math.PI/180))
    d.fn = d.fz * Math.cos(d.alpha * (Math.PI/180))
    d.µ = Math.tan(d.alpha * (Math.PI/180))

    d.vControle = (d.fw * d.t - d.fz * Math.sin(d.alpha * (Math.PI/180)) * d.t) / (-1 * d.m)
    d.sControle = d.vControle * d.t
    */

    // d.fControle = (d.fw - d.f d.fz * Math.sin(d.alpha * (Math.PI/180))) ? true : false


    /*d.t0 = d.t0 / 1000
    d.t1 = d.t1 / 1000
    d.t = d.t1 - d.t0
    d.v = s / d.t
    d.fg = d.m * 9.81
    d.fw = d.fg * Math.sin(d.alpha * (Math.PI/180))
    d.fn = d.fg * Math.cos(d.alpha * (Math.PI/180))
    d.µ = Math.tan(d.alpha * (Math.PI/180))
    d.µControle = d.fw / d.fn

    */

    //https://www.youtube.com/watch?v=8xOU25PWx8M
    //https://www.youtube.com/watch?v=iA7Thhnzc64

//    d.t0 = d.t0 / 1000
//    d.t1 = d.t1 / 1000
    d.t = d.t1 - d.t0
//    d.v = s / d.t
//    d.a = d.v / d.t
//    d.fg = d.m * 9.81
//    d.fgy = d.fg * Math.sin(50 /* degrees */ * (Math.PI/180))
//    d.fgx = d.fg * Math.cos(50 /* degrees */ * (Math.PI/180))
    d.µ = Math.tan(d.alpha * (Math.PI/180))
    d.success = true

//    d.success = ((d.µControle >= (d.µ - d.µ * 0.05) && (d.µControle <= (d.µ + d.µ * 0.05)))) ? true : false

    if ((settings.saveSuccessfulToVault && d.success) || settings.saveAllToVault) saveToVault(d.id)
    console.log(settings.saveAllToVault)
    var log = 'Calculation done'
    socket.broadcast.emit('log', log)
    socket.emit('log', log)

    socket.broadcast.emit('calculationDone', d)

  }

  function saveToVault(id) {
    console.log('test')
    fs.writeFile('./vault.txt', JSON.stringify(savedData), function (err) {
      if (err) console.log(err)
    })
  }

  socket.on('settingsChanged', (s) => {
    settings = s
  })

  socket.on('deleteTest', (id) => {
    console.log(id)
    savedData.splice(savedData.indexOf(obj => obj.id == id), 1)
    socket.emit('tests', savedData.filter(a => a.success != undefined))
    socket.broadcast.emit('tests', savedData.filter(a => a.success != undefined))
    saveToVault()
  })

  socket.on('isTester', (val) => {
    if (val) testerId = socket.id
    socket.broadcast.emit('snackbar', 'Tester Connected!')
  })

  socket.on('restart', () => {
    process.exit();
  })

  socket.on('loadTests', () => {
    fs.readFile('./vault.txt', (err, data) => {
      if (err) throw err
      savedData = JSON.parse(data).filter(obj => obj.id != undefined)
      socket.emit('tests', savedData)
      socket.broadcast.emit('tests', savedData)
    })
  })

  socket.on('disconnect', () => {
    console.log("Socket Disconnected")
    if (testerId == socket.id) socket.broadcast.emit('snackbar', 'Tester Disconnected!')
  });
});
