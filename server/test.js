
var fs = require('fs')
var express = require('express')
var app = express()
var port = 25555
var middleware = require('socketio-wildcard')();
var ex = require('./t');
var options = {
};

var server = require('http').Server(options, app)
var io = require('socket.io')(server)

server.listen(port, () => {
  console.log("Express server listening on port " + port);
});

var savedData = []
var objects = []
var defaultSettings = {
  saveSuccessfulToVault: false,
  saveAllToVault: false
}
var settings = defaultSettings

var testerId = null

var logger = fs.createWriteStream('log.txt', {
  flags: 'a' // 'a' means appending (old data will be preserved)
})

app.get('/tests', (req, res) => {
    // You can define styles as json object
    ex.export(res)
});

app.get('/console', (req, res) => {
  res.sendFile(__dirname + '/console.html');
});

app.get('*', (req, res) => {
  res.json({error: req.originalUrl + ' not found'})
})

