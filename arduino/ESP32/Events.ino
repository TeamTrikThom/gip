void connect(const char * payload, size_t length) {
  Serial.println("Verbonden met de server");
}

void disconnected(const char * payload, size_t length) {
  Serial.println("Geen verbinding met de server");
}

void testStarting(const char * payload, size_t length) {
  getLength();
  JsonObject& data = jsonBuffer.parseObject(payload);
  if(!testStarted) {
    firstAlpha = analogRead(potPin);
    id = data["id"];
    char i[200];
    data["id"].printTo(i);
    getLength();
    delay(1000);
    Wrijving.emit("testStarted", i);
    testStarted = true;
  }
}
