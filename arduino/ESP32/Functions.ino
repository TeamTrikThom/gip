

void getLength() {
  getDistance();
  Serial.print("distance: ");
  Serial.println(distance);
}

void getDistance() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  //distance = duration * 0.034 / 2;
  distance = 6.75;
  lengte = pulseIn(echoPin, HIGH)*0.034/2;
  if (DEBUG) {
    Serial.print("sensor: ");
    Serial.println(distance);
  }
}

void runTest() {
  
  
 
  int test25 = analogRead(25);
    int test33 = analogRead(33);
    int test32 = analogRead(32);
    int test34 = analogRead(34);
  if((startTime == 0) && (endTime == 0)) {
    startTime = millis();
    Serial.print("Start: ");
    Serial.print(startTime);
    Serial.println(" ms");
    alpha = analogRead(potPin);
    JsonObject& root = jsonBuffer.createObject();
    JsonObject& obj1 = jsonBuffer.createObject();
    JsonArray& data = root.createNestedArray("data");
    obj1["id"] = id;
    obj1["name"] = "t0";
    obj1["value"] = startTime;
    data.add(obj1);
    char payload[200];
    data.printTo(payload);
    Serial.println(payload);
    Wrijving.emit("addValuesToTest", payload);
    digitalWrite(motorPin, LOW);
    
  }

  if ((startTime != 0) && (endTime == 0)) {

    delay(100);
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    lengte = pulseIn(echoPin, HIGH)*0.034/2;
    Serial.print("sensor: ");
    Serial.println(lengte);
    
    alpha = analogRead(potPin);
    
    Serial.println(alpha);
    digitalWrite(motorPin, HIGH);
    digitalWrite(18, LOW);
    digitalWrite(19, HIGH);
  }

  if(((lengte > distance * 1.08)) && (startTime+1000 < millis()) && (endTime == 0)) {
    alpha = analogRead(potPin);
    Serial.println(lengte);
    Serial.println(distance);
    digitalWrite(motorPin, LOW);
    endTime = millis();
    Serial.print("Einde: ");
    Serial.print(endTime);
    Serial.println(" ms");

    Serial.print("Tijd: ");
    Serial.print(endTime - startTime);
    Serial.println(" ms");

    testStarted = false;

    endTest();
  }
}

void endTest() {
  Serial.println("test");
  Serial.println(alpha);
  Serial.println(firstAlpha);
  alpha = map(alpha, firstAlpha, firstAlpha+1030, 0, 7000) / 100.00;
  
  digitalWrite(motorPin, LOW);
  digitalWrite(18, LOW);
  digitalWrite(19, LOW);
  JsonObject& root = jsonBuffer.createObject();
  JsonObject& obj1 = jsonBuffer.createObject();
  JsonObject& obj2 = jsonBuffer.createObject();
  JsonArray& data = root.createNestedArray("data");
  obj1["id"] = id;
  obj1["name"] = "t1";
  obj1["value"] = endTime;
  data.add(obj1);
  obj2["id"] = id;
  obj2["name"] = "alpha";
  obj2["value"] = alpha;
  data.add(obj2);
  char payload[200];
  data.printTo(payload);
  char i[200];
  obj1["id"].printTo(i);
  obj2["id"].printTo(i);
  Wrijving.emit("addValuesToTest", payload);
  Wrijving.emit("testEnded", i);
  startTime = 0;
  endTime = 0;
}
