#include <ArduinoJson.h>
#include <Wrijving.h>

DynamicJsonBuffer  jsonBuffer(200);

Wrijving Wrijving;

int id;
int trigPin = 15;
int echoPin = 17;
int motorPin = 5;
int potPin = 35;


float alpha;
float firstAlpha;
float lengte;
float duration;
float distance;

long startTime = 0;
long endTime = 0;

bool testStarted;
bool DEBUG;

void setup() {
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(motorPin, OUTPUT);
  pinMode(potPin, INPUT);

  pinMode(18, OUTPUT);
  pinMode(19, OUTPUT);

  pinMode(0, INPUT);
  
  pinMode(34, INPUT);
  pinMode(32, INPUT);
  pinMode(33, INPUT);
  pinMode(25, INPUT);
  
  Serial.begin(9600);
  
  Wrijving.start("Thomas", "appelflap");

  Wrijving.on("connect", connect);
  Wrijving.on("disconnected", disconnected);
  Wrijving.on("testStarting", testStarting);
  
  Wrijving.beginSSL("dev.trikthom.com", 25555);

  while (distance > 10 || distance < 1) getLength();

}

void loop() {
  Wrijving.loop();
  digitalWrite(motorPin, HIGH);
  if(testStarted) runTest();
  if (Serial.available() > 0) DEBUG = (bool)Serial.readString();

  if(digitalRead(0) == LOW && testStarted == false) {
    digitalWrite(motorPin, HIGH);
    digitalWrite(18, HIGH);
    digitalWrite(19, LOW);
  }
  else if(testStarted == false) {
    digitalWrite(motorPin, LOW);
    digitalWrite(18, LOW);
    digitalWrite(19, LOW);  
  }
}
