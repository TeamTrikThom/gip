#include <Wrijving.h>
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti WiFiMulti;

String getEventName(String payload) {
	return payload.substring(4, payload.indexOf("\"", 4));
}

String getEventPayload(String payload) {
	String result = payload.substring(payload.indexOf("\"", 4) + 2, payload.length() - 1);
	if(result.startsWith("\"")) {
		result.remove(0,1);
	}
	if(result.endsWith("\"")) {
		result.remove(result.length() - 1);
	}
	return result;
}

void Wrijving::start(const char* ssid, const char* pass) {
	Serial.begin(115200);

    Serial.setDebugOutput(true);
    Serial.flush();
	
    WiFiMulti.addAP(ssid, pass);
    while(WiFiMulti.run() != WL_CONNECTED) {
        delay(100);
        WRIJVING_DEBUG("[Wrijving] Niet verbonden\n");
        WiFiMulti.addAP(ssid, pass);
    }
    WRIJVING_DEBUG("[Wrijving] Connected with the internet!\n");
    beginSSL("rest.trikthom.nl", 25555); // socket io server
}

void Wrijving::webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {
	String msg;
	switch(type) {
		case WStype_DISCONNECTED:
			WRIJVING_DEBUG("[Wrijving] Disconnected!\n");
            trigger("disconnected", NULL, 0);
			break;
		case WStype_CONNECTED:
			WRIJVING_DEBUG("[Wrijving] Connected to url: %s\n",  payload);
			break;
		case WStype_TEXT:
			msg = String((char*)payload);
			if(msg.startsWith("42")) {
				trigger(getEventName(msg).c_str(), getEventPayload(msg).c_str(), length);
			} else if(msg.startsWith("2")) {
				_webSocket.sendTXT("3");
			} else if(msg.startsWith("40")) {
				trigger("connect", NULL, 0);
				emit("isTester", "true");
			}
			break;
		case WStype_BIN:
			WRIJVING_DEBUG("[Wrijving] get binary length: %u\n", length);
		break;
	}
}

void Wrijving::beginSSL(const char* host, const int port, const char* url, const char* fingerprint) {
	_webSocket.beginSSL(host, port, url, fingerprint); 
    initialize();
}

void Wrijving::begin(const char* host, const int port, const char* url) {
	_webSocket.begin(host, port, url);
    initialize();
}

void Wrijving::initialize() {
    _webSocket.onEvent(std::bind(&Wrijving::webSocketEvent, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
	_lastPing = millis();
}

void Wrijving::loop() {
	_webSocket.loop();
	for(auto packet=_packets.begin(); packet != _packets.end();) {
		if(_webSocket.sendTXT(*packet)) {
			WRIJVING_DEBUG("[Wrijving] packet \"%s\" emitted\n", packet->c_str());
			packet = _packets.erase(packet);
		} else {
			++packet;
		}
	}

	if(millis() - _lastPing > PING_INTERVAL) {
		_webSocket.sendTXT("2");
		_lastPing = millis();
	}
}

void Wrijving::on(const char* event, std::function<void (const char * payload, size_t length)> func) {
	_events[event] = func;
}

void Wrijving::emit(const char* event, const char * payload) {
	String msg = String("42[\"");
	msg += event;
	msg += "\"";
	if(payload) {
		msg += ",";
		msg += payload;
	}
	msg += "]";
	WRIJVING_DEBUG("[Wrijving] add packet %s\n", msg.c_str());
	_packets.push_back(msg);
}

void Wrijving::trigger(const char* event, const char * payload, size_t length) {
	auto e = _events.find(event);
	if(e != _events.end()) {
		WRIJVING_DEBUG("[Wrijving] trigger event %s\n", event);
		e->second(payload, length);
	} else {
		WRIJVING_DEBUG("[Wrijving] event %s not found. %d events available\n", event, _events.size());
	}
}