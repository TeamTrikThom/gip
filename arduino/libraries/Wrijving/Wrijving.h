#ifndef __WRIJVING_H__
#define __WRIJVING_H__

#include <Arduino.h>
#include <map>
#include <vector>
#include <WebSocketsClient.h>
#include <WiFi.h>
#include <WiFiMulti.h>

#define WRIJVING_DEBUG(...) Serial.printf(__VA_ARGS__);
#define PING_INTERVAL 10000
#define DEFAULT_PORT 25555
#define DEFAULT_URL "/socket.io/?transport=websocket"
#define DEFAULT_FINGERPRINT ""


class Wrijving {
private:
	std::vector<String> _packets;
	WebSocketsClient _webSocket;
	int _lastPing;
	std::map<String, std::function<void (const char * payload, size_t length)>> _events;

	void trigger(const char* event, const char * payload, size_t length);
	void webSocketEvent(WStype_t type, uint8_t * payload, size_t length);
    void initialize();
public:
    void beginSSL(const char* host, const int port = DEFAULT_PORT, const char* url = DEFAULT_URL, const char* fingerprint = DEFAULT_FINGERPRINT);
	void begin(const char* host, const int port = DEFAULT_PORT, const char* url = DEFAULT_URL);
	void loop();
	void on(const char* event, std::function<void (const char * payload, size_t length)>);
	void emit(const char* event, const char * payload = NULL);
	void start(const char* ssid, const char* pass);
};

#endif