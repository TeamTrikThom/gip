#include <Wrijving.h>
Wrijving Wrijving;

void setup() {
  Serial.begin(115200);
  Wrijving.start("ssid", "pass");

  Wrijving.on("connect", connect);
  Wrijving.on("disconnected", disconnected);
  
  Wrijving.beginSSL("rest.trikthom.nl", 25555);
}

void loop() {
  Wrijving.loop();
}

void connect(const char * payload, size_t length) {
  Serial.println("Verbonden met de server");
}

void disconnected(const char * payload, size_t length) {
}