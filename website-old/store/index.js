import axios from 'axios'

export const state = () => ({
  tests: [],
  connected: false,
  loading: false,
  log: '',
  autorun: false,
  objects: [],
  settings: {},
  serverUrl: 'https://dev.trikthom.com:25555'
})

export const getters = {
  tests (state) {
    return state.tests
  },
  autorun (state) {
    return state.autorun
  },
  objects (state) {
    return state.objects
  },
  settings (state) {
    return state.settings
  }
}

export const mutations = {
  setTests (state, tests) {
    state.tests = tests
  },
  SOCKET_LOG (state, log) {
    state.log = log[0]
  },
  SOCKET_TESTS (state, tests) {
    state.tests = tests[0]
    state.connected = true
  },
  SOCKET_CONNECT (state) {
  },
  SOCKET_STARTNEWTEST (state, data) {
    state.loading = true
  },
  SOCKET_TESTSTARTING (state) {
    state.loading = true
  },
  SOCKET_CALCULATIONDONE (state, data) {
    // this.app.router.push('/Tests/' + data[0].group)
    state.tests.push(data[0])
    state.loading = false
  },
  SOCKET_AUTORUN (state, val) {
    state.loading = val
  },
  setLoading (state, p) {
    state.loading = p
  },
  SOCKET_OBJECTS (state, objects) {
    state.objects = objects[0]
    if (state.loading) state.loading = false
  },
  SOCKET_SETTINGS (state, settings) {
    state.settings = settings[0]
  }
}

export const actions = {
  init (context) {
    axios.get('/tests').then((res) => {
      return context.commit('setTests', res.data)
    })
  }
}
