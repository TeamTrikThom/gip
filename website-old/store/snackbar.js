export const state = () => ({
  value: false,
  text: ''
})

export const getters = {
  value (state) {
    return state.value
  },
  text (state) {
    return state.text
  }
}

export const mutations = {
  SOCKET_SNACKBAR (state, snackbar) {
    state.text = snackbar[0]
    state.value = true
  },
  setValue (state, v) {
    state.value = v
  }
}
