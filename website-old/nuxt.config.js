
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Wrijving',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { hid: 'description', name: 'description', content: 'GIP van Thomas Crombez en Evert Devos' },
      { name: 'description', name: 'description', content: 'GIP van Thomas Crombez en Evert Devos' },
      { lang: 'nl' }

    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' },
      { rel: 'manifest', href: '/manifest.json' }
    ]
  },
  plugins: [
    { src: '~/plugins/vuetify.js', ssr: true },
    { src: '~/plugins/socket-io.js', ssr: false },
    { src: '~/plugins/vue-chartjs.js', ssr: false }
  ],
  router: {
    middleware: []
  },
  css: [
    '~/assets/style/app.styl'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3ECE77' },
  modules: [
    '@nuxtjs/proxy'
  ],
  proxy: {
    '/tests': 'https://wrijving.trikthom.com:25555/'
  },
  /*
  ** Build configuration
  */
  build: {
    vendor: [
      'vuetify'
    ],
    extractCSS: true,
    cssSourceMap: false,
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
