export const state = () => ({
  serverUrl: 'https://dev.trikthom.com:25555',
  tests: [],
  connected: false,
  settings: {},
  loading: false,
  log: ''
})

export const getters = {
  tests (state) {
    return state.tests
  },
  settings (state) {
    return state.settings
  }
}

export const actions = {
  
}

export const mutations = {
  setTests(state, tests) {
    state.tests = tests
  },
  addTest(state, test) {
    state.tests.push(test)
  },
  connected(state, s) {
    state.connected = s
  },
  setSettings(state, settings) {
    state.settings = settings
  },
  setLoading(state, s) {
    state.loading = s
  },
  setLog(state, msg) {
    state.log = msg
  }
}