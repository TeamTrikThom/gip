export const state = () => ({
  value: false,
  text: ''
})

export const getters = {
  value (state) {
    return state.value
  },
  text (state) {
    return state.text
  }
}

export const mutations = {
  setSnackbar (state, msg) {
    state.text = msg
    state.value = true
  },
  setValue (state, v) {
    state.value = v
  }
}
