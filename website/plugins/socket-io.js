import Vue from 'vue'
import VueSocketIO from 'vue-socket.io'

export default ({ app, store }) => {
    Vue.use(new VueSocketIO({
        debug: false,
        connection: app.store.state.serverUrl,
        vuex: app.store
    }))
}
