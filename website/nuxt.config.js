module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'GIP',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { hid: 'description', name: 'description', content: 'GIP van Thomas Crombez en Evert Devos' },
      { name: 'description', name: 'description', content: 'GIP van Thomas Crombez en Evert Devos' },
      { lang: 'nl' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' },
      { rel: 'manifest', href: '/manifest.json' }
    ]
  },
  plugins: [
    { src: '~/plugins/vuetify.js', ssr: true },
    { src: '~/plugins/socket-io.js', ssr: false }
  ],
  modules: [
    '@nuxtjs/proxy'
  ],
  proxy: {
    '/backend': 'https://dev.trikthom.com:25555/'
  },
  css: ['~/assets/style/app.styl'],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3ECE77' },
  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    cssSourceMap: false
  }
}
