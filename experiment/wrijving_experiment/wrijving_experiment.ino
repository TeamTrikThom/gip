
int trigPin1 = 15;
int echoPin1 = 4;
int trigPin2 = 16;
int echoPin2 = 17;
int width = 15; //in cm
int distance1;
int distance2;

long duration1;
long duration2;
long startTime = 0;
long endTime = 0;

void setup() {
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2, INPUT);
  pinMode(2, OUTPUT);
  Serial.begin(9600);

}

void loop()
{
  digitalWrite(trigPin1, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin1, LOW);
  duration1 = pulseIn(echoPin1, HIGH);
  distance1 = duration1 * 0.034 / 2;

  digitalWrite(trigPin2, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin2, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin2, LOW);
  duration2 = pulseIn(echoPin2, HIGH);
  distance2 = duration2 * 0.034 / 2;
  
  if(((distance1 < width * 0.8) || (distance1 > width * 1.2)) && (startTime == 0) && (endTime == 0)) {
    startTime = millis();
    Serial.print("Start: ");
    Serial.print(startTime);
    Serial.println(" ms");
    //Serial.print("s1: ");
    //Serial.println(distance1);
  }

  if(((distance2 < width * 0.8) || (distance2 > width * 1.2)) && (startTime != 0) && (endTime == 0)) {
    endTime = millis();
    Serial.print("Einde: ");
    Serial.print(endTime);
    Serial.println(" ms");

    Serial.print("Tijd: ");
    Serial.print(endTime - startTime);
    Serial.println(" ms");
    //Serial.print("s2: ");
    //Serial.println(distance2);
    startTime = 0;
    endTime = 0;
  }
}

